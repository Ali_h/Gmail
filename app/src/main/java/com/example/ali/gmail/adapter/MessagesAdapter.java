package com.example.ali.gmail.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.ali.gmail.R;
import com.example.ali.gmail.helper.CircularTransform;
import com.example.ali.gmail.helper.FlipAnimator;
import com.example.ali.gmail.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 2/22/18.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {

    private Context context;
    private List<Message> messages;
    private SparseBooleanArray selectedItem;

    // array used to perform multiple animation at once
    private SparseBooleanArray animationItemsIndex;
    private boolean reverseAllAnimations = false;

    // index is used to animate only the selected row
    private static int currentSelectedIndex = -1;

    public interface MessageAdapterListener {
        void onIconClicked(int position);

        void onIconImportantClicked(int position);

        void onMessageRowClicked(int position);

        void onRowLongClicked(int position);
    }

    private MessageAdapterListener listener;

    public MessagesAdapter(Context context, List<Message> messages, MessageAdapterListener listener) {
        this.context = context;
        this.messages = messages;
        this.listener = listener;
        selectedItem = new SparseBooleanArray();
        animationItemsIndex = new SparseBooleanArray();
    }

    @Override
    public MessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessagesAdapter.ViewHolder holder, int position) {
        Message message = messages.get(position);

        // displaying text view data
        holder.from.setText(message.getFrom());
        holder.subject.setText(message.getSubject());
        holder.message.setText(message.getMessage());
        holder.timeStamp.setText(message.getTimestamp());

        // displaying the first letter of From in icon text
        holder.iconText.setText(message.getFrom().substring(0, 1));//end index ro dar nazar nemigire

        // change the row state to activated ??????????????????????????????????????????/
        holder.itemView.setActivated(selectedItem.get(position, false));

        // change the font style depending on message read status
        applyReadStatus(holder, message);

        // handle message star
        applyImportant(holder, message);

        // handle icon animation
        applyIconAnimation(holder, position);

        // display profile image
        applyProfilePicture(holder, message);

        // apply click events
        applyClickEvents(holder, position);

    }

    private void applyClickEvents(ViewHolder holder, final int position) {
        holder.iconContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onIconClicked(position);
            }
        });

        holder.iconImp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onIconImportantClicked(position);
            }
        });

        holder.messageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onMessageRowClicked(position);
            }
        });

        holder.messageContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onRowLongClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                //https://stackoverflow.com/questions/2830044/android-haptic-feedback-onclick-event-vs-hapticfeedbackenabled-in-the-view?rq=1
                return false;
            }
        });
    }

    private void applyProfilePicture(ViewHolder holder, Message message) {
        if (!TextUtils.isEmpty(message.getPicture())) {
            Glide.with(context).load(message.getPicture())
                    .thumbnail(0.5f)//lide will display the original image reduced to 50% of the size.
                    .crossFade()
                    .transform(new CircularTransform(context))//inja oon transformi ke too bitmap besh dadim ro besh midim
                    //https://futurestud.io/tutorials/glide-custom-transformation
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //https://futurestud.io/tutorials/glide-caching-basics
                    .into(holder.imgProfile);
            holder.imgProfile.setColorFilter(null);
            holder.iconText.setVisibility(View.GONE);
        } else {
            holder.imgProfile.setImageResource(R.drawable.bg_circle);
            holder.imgProfile.setColorFilter(message.getColor());
            holder.iconText.setVisibility(View.VISIBLE);
        }
    }

    private void applyIconAnimation(ViewHolder holder, int position) {
        if (selectedItem.get(position, false)) {
            holder.iconFront.setVisibility(View.GONE);
            resetIconYAxis(holder.iconBack);
            holder.iconBack.setVisibility(View.VISIBLE);
            holder.iconBack.setAlpha(1);
            if (currentSelectedIndex == position) {
                FlipAnimator.flipView(context, holder.iconBack, holder.iconFront, true);
                resetCurrentIndex();
            }
        } else {
            holder.iconBack.setVisibility(View.GONE);
            resetIconYAxis(holder.iconFront);
            holder.iconFront.setVisibility(View.VISIBLE);
            holder.iconFront.setAlpha(1);
            if ((reverseAllAnimations && animationItemsIndex.get(position, false)) || currentSelectedIndex == position) {
                FlipAnimator.flipView(context, holder.iconBack, holder.iconFront, false);
                resetCurrentIndex();
            }
        }
    }

    // As the views will be reused, sometimes the icon appears as
    // flipped because older view is reused. Reset the Y-axis to 0
    private void resetIconYAxis(View view) {
        if (view.getRotationY() != 0) {
            view.setRotationX(0);
        }
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    private void applyImportant(ViewHolder holder, Message message) {
        if (message.isImportant()) {
            holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));
            holder.iconImp.setColorFilter(ContextCompat.getColor(context, R.color.icon_tint_selected));//for colored the image
        } else {
            holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));
            holder.iconImp.setColorFilter(ContextCompat.getColor(context, R.color.icon_tint_normal));//for colored the image
        }
    }

    private void applyReadStatus(ViewHolder holder, Message message) {
        if (message.isRead()) {
            holder.from.setTypeface(null, Typeface.NORMAL);
            holder.subject.setTypeface(null, Typeface.NORMAL);
            holder.from.setTextColor(ContextCompat.getColor(context, R.color.from));//for get color in color foolder
            holder.subject.setTextColor(ContextCompat.getColor(context, R.color.subject));
        } else {
            holder.from.setTypeface(null, Typeface.BOLD);
            holder.subject.setTypeface(null, Typeface.BOLD);
            holder.from.setTextColor(ContextCompat.getColor(context, R.color.from));
            holder.subject.setTextColor(ContextCompat.getColor(context, R.color.subject));
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public long getItemId(int position) {
        return messages.get(position).getId();
    }

    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItem.get(pos, false)) {
            selectedItem.delete(pos);
            animationItemsIndex.delete(pos);
        } else {
            selectedItem.put(pos, true);
            animationItemsIndex.put(pos, true);
        }
        notifyDataSetChanged();
    }

    public void clearSelections() {
        reverseAllAnimations = true;
        selectedItem.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItem.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItem.size());
        for (int i = 0; i < selectedItem.size(); i++) {
            items.add(selectedItem.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        messages.remove(position);
        resetCurrentIndex();
    }

    public void resetAnimationIndex() {
        reverseAllAnimations = false;
        animationItemsIndex.clear();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        private TextView from, subject, message, iconText, timeStamp;
        private ImageView imgProfile, iconImp;
        private LinearLayout messageContainer;
        private RelativeLayout iconContainer, iconFront, iconBack;

        public ViewHolder(View itemView) {
            super(itemView);
            from = itemView.findViewById(R.id.from);
            subject = itemView.findViewById(R.id.text_primary);
            message = itemView.findViewById(R.id.text_secondary);
            iconText = itemView.findViewById(R.id.icon_text);
            timeStamp = itemView.findViewById(R.id.timestamp);

            imgProfile = itemView.findViewById(R.id.icon_profile);
            iconImp = itemView.findViewById(R.id.icon_star);

            messageContainer = itemView.findViewById(R.id.message_container);

            iconContainer = itemView.findViewById(R.id.icon_container);
            iconBack = itemView.findViewById(R.id.icon_back);
            iconFront = itemView.findViewById(R.id.icon_front);

            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View v) {
            listener.onRowLongClicked(getAdapterPosition());
            itemView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            return false;
        }
    }
}
