package com.example.ali.gmail.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

/**
 * Created by ali on 2/22/18.
 */

public class CircularTransform extends BitmapTransformation {
    private static final String TAG = "CircularTransform";

    public CircularTransform(Context context) {
        super(context);
    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        return circleCrop(pool, toTransform);
    }

    private static Bitmap circleCrop(BitmapPool pool, Bitmap source) {
        if (source == null)
            return null;

        int size = Math.min(source.getWidth(), source.getHeight());
        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;
        Log.d(TAG, "circleCrop: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx : " + x);
        Log.d(TAG, "circleCrop: yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy : " + y);

        Bitmap squared = Bitmap.createBitmap(source, x, y, size, size);

        Bitmap resualt = pool.get(size, size, Bitmap.Config.ARGB_8888);
        if (resualt == null) {
            Log.d(TAG, "circleCrop: bitmap is nullllllllllllllllllllllllllllllllll");
            resualt = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(resualt);
        Paint paint = new Paint();
        paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
        paint.setAntiAlias(true);
        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);
        return resualt;


    }

    @Override
    public String getId() {
        return getClass().getName();
    }
}
